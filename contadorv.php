<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        
.views {
    color: #ff0000;
}
.total {
    font-family: arial;
    font-size: 1em;
    vertical-align: middle;
}

#contentt2 {
    padding: 5px;
    margin: 5px auto;
    cursor: pointer;
    appearance:button;
    -moz-appearance:button; /* Firefox */
    -webkit-appearance:button; /* Safari and Chrome */
}
#contentt2 span {
    -moz-user-select: none; /* Firefox */
    -webkit-user-select: none; /* Safari y Chrome */
}
    </style>
    <script>

    $(document).ready(function(){
        // Hacemos la Function para abrir el contador de visitas
function recargar_views(){
    $(".views").load("contador.txt");
}

// Establecemos el temporizador a 1 segundos
timer = setInterval("recargar_views()", 1000);

});
    </script>
</head>

<?php
$archivo = "contador.txt"; // Archivo en donde se acumulará el numero de visitas
$abre = fopen($archivo, "r"); // Abrimos el archivo para solamente leerlo (r de read)
$total = fread($abre, filesize($archivo)); // Leemos el contenido del archivo(filesize "detectara" la longitud de Bytes de $archivo la cual desconocemos)
fclose($abre); // Cerramos la conexión al archivo
$abre = fopen($archivo, "w"); // Abrimos nuevamente el archivo (w de write)
$total = $total + 1; // Sumamos 1 nueva visita
$grabar = fwrite($abre, $total); // Y reemplazamos por la nueva cantidad de visitas
fclose($abre); // Cerramos la conexión al archivo

// Imprimimos el total de visitas dándole un formato
echo "<div id='contentt2'><img src='img/views.png' alt='' width= '32px' /><span class='total'>Total de visitas: <span class='views'>".$total."</span></span></div>";
?>


<body>
    
</body>
</html>