<html>
<head>
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <script src="jquery/jquery.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
</head>	
<body>
	<h2>Especialidades</h2>
  <?php
  $con = mysqli_connect("localhost", "cipisof1_icatlax", "1c47l4x2017", "cipisof1_icatlax");
  //$con = mysqli_connect("localhost", "root", "necaxa", "ICAT");
  
// Check connection
  if (mysqli_connect_errno())
  {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$utf = $con->query("SET NAMES 'utf8'");//Respetar UTF-8
$sql="SELECT * FROM Especialidades";

$result = $con->query($sql);

if ($result->num_rows > 0) {
  echo "<select id='espe' class='form-control'>";
  $j=1;
  while($row = $result->fetch_assoc()) {
    echo "<option value=".$j++.">".$row['nombre']."</option>";
  }
  echo "</select></br></br> <h3>Planteles donde se Imparte:</h3>";

  $result = $con->query($sql);
  
  $j=1;
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      
      $sql2="SELECT Plantel.id, Plantel.nombre FROM Plantel, ofertaAcademica WHERE ofertaAcademica.id_especialidad=".$row['id']." AND ofertaAcademica.id_plantel=Plantel.id;";
    	//$sql2="SELECT * FROM Cursos WHERE id_especialidad=".$row['id'].";";
      $result2 = $con->query($sql2);

      if ($result2->num_rows > 0) {
        echo "<div class='cursos' id='div".$j++."' style='display:none;'>";
        while ($row2 = $result2->fetch_assoc()){
         echo "
         <ul class='list-group'>
          <li class='list-group-item'><a href='planteles.php?vId=".$row2['id']."'>".$row2['nombre']."</a></li>
        </ul>
        ";
      }
      echo "</div>";
    }
    
  }
}
} else {
  echo "0 results";
}

$con->close();

?>
<script>
	$(document).ready(function(){
		$('#div1').show();
		$('#espe').click(function(){
			var opci=$(this).val();
			$(".cursos").hide();
			$("#div"+opci).show();
		});
	});
</script>
</body>
</html>