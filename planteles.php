<!DOCTYPE html>
<html lang="en">
<head>
  <title>Instituto de capacitación para el Trabajo del Estado de Tlaxcala</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="jquery/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
    }
  </style>
  <script>
    $(document).ready(function(){
      if($('#idPlan').val()!=""){
        $('#plantelX').load("oferta_caplulalpan.php",{plantel:$('#idPlan').val()});
      }
      $("#plantelX").show();   
      $('.plant').click(function(){
        var opcion1=$(this).attr("id");
        var opNum;
        switch(opcion1){
          case "calpulalpan":
          opNum=1;
          break;
          case "chiahutempan":
          opNum=2;
          break;
          case "huamantla":
          opNum=3;
          break;
          case "tetla":
          opNum=4;
          break;
          case "tepetitla":
          opNum=5;
          break;
          case "tetlanohcan":
          opNum=6;
          break;
          case "tlaxco":
          opNum=7;
          break;
          case "sanpablo":
          opNum=8;
          break;
          case "zitlaltepec":
          opNum=9;
          break;
          case "papalotla":
          opNum=10;
          break;
        }        
        $('#plantelX').load("oferta_caplulalpan.php",{plantel:opNum});
      });

    });
  </script>
</head>
<body >
<input type="hidden" id='idPlan' value='<?php echo $_GET["vId"];?>'>
  <div class="container">
    <div class="panel panel-default"><!-- ################2 DIVS INICIALES###############-->
      <div class="panel-heading"><!-- ################PANEL ENCABEZADO################-->
        <!-- ########################################       ENCABEZADO    #################################################################-->
        <div class="row container-fluid">
          <div class="col-md-4 col-xs-12"></div>
          <div class="col-md-4 col-xs-12"></div>
          <div  class="col-md-4 col-xs-12"> 
            <a href="#" >Portal Ciudadano</a> | <a href="#" > Directorio</a> | <a href="#" >Gobierno de Tlaxcala</a> | <a href="#" >Contacto</a>
          </div>
        </div> 
        <div class="row container-fluid">
          <div class="col-md-4 col-xs-12">
            <img class="imgbanner img-responsive" src="img/ICATLAX.png" alt="Gobierno del Estado de Tlaxcala">
          </div>
          <div class="col-md-4 col-xs-12"></div>
          <div class="col-md-4 col-xs-12"></div>
        </div> 
      </div><!--PANEL ENCABEZADO-->
      <div class="panel-body"><!-- ################PANEL BODY################-->
        <!-- ####################NAVBAR ################-->
        <nav class="navbar">
          <div class="container-fluid">
            <ul class="nav navbar-nav">
              <li><a id="menu" href="index.html">ICATLAX</a>
                <li><a id="menu" href="about.html">Nosotros</a></li>
                <li><a id="menu" href="oferta.html">Oferta</a></li>
                <li><a id="menu" href="planteles.php">Planteles</a></li>
                <li><a id="menu" href="contacto.html">Contácto</a></li>
                <li><a id="menu" href="http://transparencia.tlaxcala.gob.mx/?dep=24&option=com_content&view=article&id=34&Itemid=73&nomdep=INSTITUTO+DE+CAPACITACI%C3%93N+PARA+EL+TRABAJO+DEL+ESTADO+DE+TLAXCALA.&ret=%2Fsistemas%2Ftransparencia%2Ftrans_qonsulta1.php&cmbDepCentral=0&cmbDepDescentral=24&cmbDepDesconce=0&cmbDepOtros=0" target="_blank">Transparencia</a></li>
                <li><a id="menu" href="http://icatlax.edu.mx/Cuenta-Publica/" target="_blank">Cuenta Pública</a></li>
              </ul>
            </div>
          </nav>
          <!-- #########################       CONTENT   ########################################-->
          <div class="row container-fluid">
            <h1>Instituto de capacitación para el Trabajo del Estado de Tlaxcala</h1>  
            <!-- ######################    IZQUIERDA   #################################################-->
            <div class="col-md-4 col-xs-12">
              <div class="list-group">
                <label id="frojo" class="list-group-item ">PLANTELES</label>
                <a href="#" class="plant list-group-item" id="calpulalpan">Plantel Calpulalpan</a>
                <a href="#" class="plant list-group-item" id="chiahutempan">Plantel Chiautempan</a>
                <a href="#" class="plant list-group-item" id="huamantla">Plantel Huamantla</a>
                <a href="#" class="plant list-group-item" id="tetla">Acción Móvil Papalotla</a>
                <a href="#" class="plant list-group-item" id="tepetitla">Plantel San Pablo del Monte</a>
                <a href="#" class="plant list-group-item" id="tetlanohcan">Plantel Tepetitla</a>
                <a href="#" class="plant list-group-item" id="tlaxco">Plantel Tetla</a>
                <a href="#" class="plant list-group-item" id="sanpablo">Plantel Tetlanohcan</a>
                <a href="#" class="plant list-group-item" id="zitlaltepec">Plantel Tlaxco</a>
                <a href="#" class="plant list-group-item" id="papalotla">Plantel Zitlaltepec</a>
              </div>
            </div>
            <!-- ##########################    CENTRO   ##########################################################-->
            <div class="calpulalpan col-md-8 col-xs-12" id="plantelX" ><!-- #######Plantel Calpulalpan######--> 

            </div> <!-- ##Plantel Calpulalpan##-->
          </div> <!-- ##############DIV CONTENT#############-->
        </div><!-- ################PANEL BODY################-->      
        <div id="footer" class="panel-footer col-md-12 col-xs-12"><!-- ################PANEL FOOTER################-->
          <!--##################################FOOTER#####################################################-->
          <div class="col-xs-12 col-lg-6 text-left">
           <p>Dirección: Antiguo Camino Real S/N, Interior del Jardín Botánico, Tizatlán, Tlax.</p>
           <p>C.P. 90100, Teléfono: (246) 466 07 11, (246) 466 07 36 </p> 
         </div>
         <div class="col-xs-12 col-lg-6 text-left">
          <p>Correo Electrónico: coordinacion.sepuede@gmail.com</p>
          <p>Horario de atención: Lunes-Viernes de 08:00-14:00 y 15:00-17:00 hrs.</p>
        </div>
        <div class="col-xs-12 col-lg-12 text-center">
          <p>(c) Copyright Icatlax 2017, Website designed by <a href="#">CIPI SOFTWARE</a></p>
        </div>
      </div>
    </div><!-- ###############2 DIVS FINALES##############-->
  </div>
</body>
</html>
